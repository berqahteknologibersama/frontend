import { Dispatch, createContext, useReducer } from "react";

type User = {
  id : string,
  name : string,
  user_id : string,
  email : string,
  login_token : string
}

type StateType = {
  loggedin : boolean;
  user_data : User | null
};

const initialState: StateType = {
    loggedin : false,
    user_data : null
};


type ReducerProps = {
    type : string
    payload : User | null
}

const reducer = (state: StateType, action:ReducerProps) => {
    switch (action.type) {
        case "LOGIN":
            // Do something when user is loggedin
            return { loggedin: !state.loggedin, user_data : action.payload };
        case "LOGOUT":
            // Do something when user is loggedout
            return { loggedin: !state.loggedin, user_data : null };
        default:
            throw new Error();            
    }
};

export const AuthContext = createContext<{
  state: StateType;
  dispatch: Dispatch<ReducerProps>;
}>({ state: initialState, dispatch: () => null });


export const AuthContextProvider = ({
  children
}: {
  children: React.ReactNode;
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};
