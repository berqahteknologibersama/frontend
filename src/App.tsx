import {Routes , Route } from "react-router-dom" 
import Navbar from './components/global/Navbar'

import Home from './components/Home'
import Login from './components/Login'
import Device from './components/Device'
import Signup from './components/Signup'


function App() {
  return (
    <>
      <Navbar/>

        <Routes> 
            <Route path="/" element={<Home/> } /> 
            <Route path="/login" element={<Login/> } /> 
            <Route path="/device/:device_type/:device_id" element={<Device/> } /> 
            <Route path="/signup" element={<Signup/> } /> 

       </Routes> 
    </>
  );
}

export default App;
