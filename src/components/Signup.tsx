import { useContext, useState  } from "react";
import { AuthContext } from "../context/Auth.context";
import {
    Input,
    Button,
    Alert
  } from "@material-tailwind/react";
import { Formik, Field, Form, FieldProps, ErrorMessage } from 'formik';
import { ExclamationTriangleIcon } from "@heroicons/react/24/solid";
import * as Yup from 'yup';
import { POST } from "../services/Fetch";
import { useNavigate } from 'react-router-dom';

export const SignupValidation = Yup.object().shape({
    user_id : Yup.string().trim().required('Required').min(3, "Username terlalu singkat").max(100, 'Username terlalu panjang'),
    password: Yup.string()
        .min(6, 'Password minimal 6 - 30 karakter')
        .max(30, 'Password terlalu panjang')
        .required('Required'),
    email: Yup.string().trim().required('Required').matches( /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Alamat email tidak valid"),
    name : Yup.string().trim().required('Required').min(3, "Masukan nama lengkap sesuai KTP").max(100, 'Nama terlalu panjang'),
});

type SignupValues = {
    user_id: string;
    email: string;
    password: string;
    name: string;
}
function Signup() { 
    const _error_modal = {
        open : false,
        message : ""
    };
    const [errorModal, setAlertModal] = useState(_error_modal);
    const navigate = useNavigate();
    const { state:AuthState, dispatch:AuthDispatch } = useContext(AuthContext);
    if(AuthState.loggedin && AuthState.user_data) navigate("/", {replace:true})

    const FailedModal = () => {
        return (
            <Alert
                    variant="gradient"
                    className="my-4"
                    color="red"
                    open={errorModal.open}
                    icon={<ExclamationTriangleIcon className="h-6 w-6" />}
                    action={
                    <Button
                        variant="text"
                        color="white"
                        size="sm"
                        className="!absolute top-3 right-3"
                        onClick={() => setAlertModal({
                            open:false,
                            message : ""
                        })}
                    >
                        Close
                    </Button>
                }
            >
                {errorModal.message}
            </Alert> 
    )
    }

    return (
        <div className="mx-auto max-w-screen-md py-12">
            <Formik
                initialValues={{
                    name: '',
                    user_id:'',
                    email: '',
                    password: '',
                }}
                validationSchema={SignupValidation}
                onSubmit={
                    async (values: SignupValues, { setSubmitting, resetForm }) => {
                        setSubmitting(true);
                        POST('http://localhost:9000/signup', {
                            name : values.name,
                            email : values.email,
                            user_id : values.user_id,
                            password : values.password
                        })
                        .then(res => {
                            if(res.status === 400) setAlertModal({open:true, message:res.error});
                            else {
                                resetForm();
                                navigate('/login')
                            }
                        })
                    }
                }
            >
            {formik => (
                <Form className="w-full">
                
                    <FailedModal />

                    <div className="my-4 flex flex-col gap-6">
                        <Field id="name" name="name">
                            {({ field }:FieldProps) => (
                            <Input color="light-blue" size='lg' {...field} label="Nama Lengkap" error={
                                formik.errors.name && formik.touched.name ? true : false
                            } />
                            )}
                        </Field>
                        <ErrorMessage name="name" component="span" className='-mt-5 text-xs' />
                        <Field id="email" name="email">
                            {({ field }:FieldProps) => (
                            <Input color="light-blue" size='lg' {...field} label="Alamat Email" error={
                                formik.errors.email && formik.touched.email ? true : false
                            } />
                            )}
                        </Field>       
                        <ErrorMessage name="email" component="span" className='-mt-5 text-xs' />
                        <Field id="user_id" name="user_id">
                            {({ field }:FieldProps) => (
                            <Input color="light-blue" size='lg' {...field} label="Username" error={
                                formik.errors.user_id && formik.touched.user_id ? true : false
                            } />
                            )}
                        </Field>       
                        <ErrorMessage name="user_id" component="span" className='-mt-5 text-xs' />                        
                        <Field id="password" name="password">
                            {({ field }:FieldProps) => (
                            <Input color="light-blue" size='lg' {...field} label="Password" type='password' error={
                                formik.errors.password && formik.touched.password ? true : false
                            } />
                            )}
                        </Field>
                        <ErrorMessage name="password" component="span" className='-mt-5 text-xs' />
                    </div>

                    <Button type="submit" className="mt-6" fullWidth disabled={formik.isSubmitting || !(formik.isValid && formik.dirty)}>
                        Sign Up
                    </Button>                                
                </Form>
            )}
            </Formik>       
        </div> 
    )
} 
export default Signup; 