import {
  Typography,
  Card,
  List,
  ListItem
} from "@material-tailwind/react";
import { AuthContext } from "../context/Auth.context";
import { useContext, useEffect, useState } from "react";
import { GET } from "../services/Fetch";
import { Link } from "react-router-dom";

type Device = {
  device_id : string
  device_type : string
  last_seen : Date
}

function Home() { 
  const { state:AuthState } = useContext(AuthContext);
  const [devices, setDevices] = useState<Device[]>([]);

  useEffect(() => {
    if(AuthState.loggedin && AuthState.user_data?.login_token){
      GET('http://localhost:9000/gps', AuthState.user_data.login_token)
      .then(res => {
          if(res.status === 200){
            setDevices(res.data)
          }
      })
    }

  }, [AuthState]);


  if(!AuthState.loggedin && !AuthState.user_data) return (
    <>
        <div className="mx-auto max-w-screen-md py-12">
        <Typography variant="h2" color="blue-gray" className="mb-2">
          Login dulu!
        </Typography>
        <Typography variant="lead" color="blue-gray" className="mb-2">
          Kamu gak bisa lihat data dihalaman ini karena belum login... Klik tombol di kanan atas
        </Typography>
      </div>    
    </>
  )

  if(devices.length === 0) return <div>No Data</div>

    return ( 
        <div className="mx-auto max-w-screen-md py-12">
        <Typography variant="h2" color="blue-gray" className="mb-2">
          Devices
        </Typography>
        <Card className="w-full rounded-none">
          <List>
          {devices.map(({device_id, device_type, last_seen}:Device) => (
            <ListItem key={device_id}>
              <Link to={`/device/${device_type.toLowerCase()}/${device_id}`}>
                <div>
                  <Typography variant="h6" color="blue-gray">
                    {device_id} - {device_type}
                  </Typography>
                  <Typography variant="small" color="gray" className="font-normal">
                    Last Seen at {new Date(last_seen).toLocaleString()}
                  </Typography>
                </div>              
              </Link>
            </ListItem>
          ))}
          </List>
        </Card>
      </div>

    ); 
} 
export default Home; 