import { useContext, useState  } from "react";
import { AuthContext } from "../context/Auth.context";
import {
    Card,
    Typography,
    Input,
    Button,
    Alert
  } from "@material-tailwind/react";
import { Formik, Field, Form, FieldProps, ErrorMessage } from 'formik';
import { ExclamationTriangleIcon } from "@heroicons/react/24/solid";
import * as Yup from 'yup';
import { Link } from "react-router-dom";
import { POST } from "../services/Fetch";
import { useNavigate } from 'react-router-dom';

export const LoginValidation = Yup.object().shape({
    password: Yup.string()
        .min(6, 'Password minimal 6 - 30 karakter')
        .max(30, 'Password terlalu panjang')
        .required('Required'),
    username : Yup.string().trim().required('Required').min(3, "Username terlalu pendek").max(100, 'Username terlalu panjang'),
});

type AuthValues = {
    username: string;
    password: string;
}
function Login() { 
    const navigate = useNavigate();

    const _error_modal = {
        open : false,
        message : ""
    };
    
    const [errorModal, setAlertModal] = useState(_error_modal);    
    const { state:AuthState, dispatch:AuthDispatch } = useContext(AuthContext);
    if(AuthState.loggedin && AuthState.user_data) return <div>Kamu udah login, ngapain login lagi coba...</div>

    const FailedModal = () => {
        return (
            <Alert
                    variant="gradient"
                    className="my-4"
                    color="red"
                    open={errorModal.open}
                    icon={<ExclamationTriangleIcon className="h-6 w-6" />}
                    action={
                    <Button
                        variant="text"
                        color="white"
                        size="sm"
                        className="!absolute top-3 right-3"
                        onClick={() => setAlertModal({
                            open:false,
                            message : ""
                        })}
                    >
                        Close
                    </Button>
                }
            >
                {errorModal.message}
            </Alert> 
        )
    }

    return ( 
    <div className="mx-auto max-w-screen-md py-12">
    <Card color="transparent" shadow={false}>
      <Typography variant="h4" color="blue-gray">
        Sign In
      </Typography>

      <FailedModal />

      <Formik
            initialValues={{
                username: '',
                password: '',
            }}
            validationSchema={LoginValidation}
            onSubmit={
                async (values: AuthValues, { setSubmitting }) => {
                    setSubmitting(true);
                    POST('http://localhost:9000/login', {
                        username : values.username,
                        password : values.password
                    })
                    .then(res => {
                        if(res.status === 400) setAlertModal({open:true, message:res.error});
                        else {
                            AuthDispatch({type: 'LOGIN', payload : res.data})
                            navigate('/', {replace:true})
                        }
                    })
  
                }
            }
        >
        {formik => (
            <Form className="mt-8 mb-2 w-full">
                <div className="mb-4 flex flex-col gap-6">
                    <Field id="username" name="username">
                        {({ field }:FieldProps) => (
                        <Input size='lg' {...field} label="Email atau Username" error={
                            formik.errors.username && formik.touched.username ? true : false
                        } />
                        )}
                    </Field>
                    <ErrorMessage name="username" component="span" className='-mt-5 text-xs' />
                    <Field id="password" name="password">
                        {({ field }:FieldProps) => (
                        <Input size='lg' {...field} label="Password" type='password' error={
                            formik.errors.password && formik.touched.password ? true : false
                        } />
                        )}
                    </Field>
                    <ErrorMessage name="password" component="span" className='-mt-5 text-xs' />
                </div>
                <Button type="submit" className="mt-6" fullWidth disabled={formik.isSubmitting || !(formik.isValid && formik.dirty)}>
                    Sign In
                </Button>                
                <Typography color="gray" className="mt-4 text-center font-normal">
                    Belum punya akun? {" "}
                    <Link to="/signup">Register</Link>
                </Typography>                                         
            </Form>
        )}
        </Formik>        
    </Card>
    </div>

    ); 
} 
export default Login; 