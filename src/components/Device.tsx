import {
  Typography,
  Timeline,
  TimelineItem,
  TimelineConnector,
  TimelineHeader,
  TimelineIcon,
  TimelineBody,
  Spinner,
  Chip
} from "@material-tailwind/react";
import { AuthContext } from "../context/Auth.context";
import { useContext, useEffect, useState } from "react";
import { GET } from "../services/Fetch";
import { PieChart } from "react-minimal-pie-chart";

import {
  useParams
} from "react-router-dom";
type Device = {
  device_id : string
  device_type : string
  timestamp : Date
  location : string
}

type GroupLocation = {
  location : string
  count : 0
}

type PieChartData = {
  title : string
  value : number
  color : string
}

function Device() { 
  const { device_type, device_id } = useParams();
  const { state:AuthState } = useContext(AuthContext);
  const [data, setDatas] = useState<Device[]>([]);
  const [isCompleted, setIsCompleted] = useState<boolean>(false);
  const [chartData, setChartData] = useState<PieChartData[]>([]);

  useEffect(() => {
    if(AuthState.loggedin && AuthState.user_data?.login_token){
      GET('http://localhost:9000/gps/'+device_id, AuthState.user_data.login_token)
      .then(res => {
          if(res.status === 200){
            // Ambil datanya, kemudian di group dijadiin chart
            setDatas(res.data);
            setIsCompleted(true);
          }
      })
    }
  }, [AuthState]);

  useEffect(() => {
    if(data.length > 0){
      let pieChartData:Array<PieChartData> = [];
      const counts = data.reduce((result: any, value) => {
        var location = value.location;
        if (!result.hasOwnProperty(location)) {
          result[location] = 0;
        }
        result[location]++;
        return result;        
      }, {});
      Object.keys(counts).map((key) => {
        pieChartData.push({
          title :key,
          value:counts[key],
          color: '#'+Math.floor(Math.random()*16777215).toString(16)
        })
      });
      setChartData(pieChartData)
    }

  }, [data]);
  
    const Page = () => {
      if(!isCompleted) return <Spinner className="h-12 w-12" />
      
      if(isCompleted && data.length === 0) return (
          <div>
            Tidak ada Data
          </div>
        );

      return (
        <div className="flex mt-4 gap-4">
            <div className="w-96">
            <Timeline>
              {data.map(({timestamp, location}:Device) => (
              <TimelineItem>
                <TimelineConnector />
                <TimelineHeader className="h-3">
                  <TimelineIcon />
                  <Typography variant="h6" color="blue-gray" className="leading-none">
                    {location}
                  </Typography>
                </TimelineHeader>
                <TimelineBody className="pb-8">
                  <Typography
                    variant="small"
                    color="gary"
                    className="font-normal text-gray-600"
                  >
                    {new Date(timestamp).toLocaleString()}
                  </Typography>
                </TimelineBody>
              </TimelineItem>
              ))}
            </Timeline>              
            </div>
            <div className="grow">
              <Typography variant="h4" color="blue-gray" className="mb-2">
                Time spent on location
              </Typography>
              <hr/>
              {
                chartData.length > 0 ? (
                <div className="flex items-start gap-4 mt-4">
                  <PieChart
                  className="w-96"
                    label={({ dataEntry }) => `${Math.round(dataEntry.percentage)} %`}
                    data={chartData}
                  />
                  <div className="grow-0">
                  <Typography variant="h6" className="mb-2">
                    Legend
                  </Typography>
                  {chartData.map(({title, color, value}:PieChartData) => (
                    <Chip 
                      className="mb-4"
                      style={{
                        backgroundColor: color,
                      }}
                      value={`${title} : ${value}`} />
                    ))}
                  </div>
                </div>
                ) : <Spinner className="h-12 w-12" />
              }

            </div>
        </div>

      )
    }
    
    return ( 
        <div className="mx-auto container py-12">
        <Typography variant="h2" color="blue-gray" className="mb-2">
          {device_id}
        </Typography>
        <Typography variant="lead" color="blue-gray" className="mb-2 capitalize">
          {device_type}
        </Typography>
        <hr className="my-4" />
        <Page/>

      </div>

    ); 
} 
export default Device; 