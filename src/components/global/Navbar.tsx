import { useContext  } from "react";
import { AuthContext } from "../../context/Auth.context";

import {
  Navbar,
  Typography,
  Button,
} from "@material-tailwind/react";
import { Link } from "react-router-dom";

export default function HeaderNavbar() {
  const { state:AuthState, dispatch:AuthDispatch } = useContext(AuthContext);
 
  const LoginInfo = () => {
    if(!AuthState.loggedin && !AuthState.user_data) return null;
    return (
        <div className="mb-4 mt-2 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6">
        <Typography
          variant="small"
          color="blue-gray"
          className="p-1 font-normal"
        >
            Halo {AuthState.user_data?.name} (@{AuthState.user_data?.user_id})
        </Typography>
      </div>           
    )
  }
 
  return (
    <>
      <Navbar className="sticky top z-10 h-max max-w-full rounded-none py-2 px-4 lg:px-8 lg:py-4">
        <div className="flex items-center justify-between text-blue-gray-900">
          <Typography
            as={Link}
            to="/"
            className="mr-4 cursor-pointer py-1.5 font-medium"
          >
            Penisieve
          </Typography>
          <div className="flex items-center gap-4">
            <div className="mr-4 hidden lg:block">
                <LoginInfo/>
            </div>
            {
                AuthState.loggedin && AuthState.user_data ? (
                    <>
                        <Button
                            variant="gradient"
                            color="red"
                            size="sm"
                            className="hidden lg:inline-block"
                            onClick={
                                () => AuthDispatch({type: 'LOGOUT', payload : null})
                            }
                        >
                        <span>Sign Out</span>
                        </Button>                    
                    </>
                ) : (
                    <>
                        <Link to="/login">
                        <Button
                            variant="gradient"
                            size="sm"
                            className="hidden lg:inline-block"
                            // onClick={
                            //     () => AuthDispatch({type: 'LOGIN', login_token: "1234"})
                            // }                            
                        >
                        <span>Sign In</span>
                        </Button>    
                        </Link>                
                    </>
                )
            }

          </div>
        </div>
      </Navbar>
    </>
  );
}