
export const POST = async (url:string, payload:any, token?:string|null) => {
    let options = {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    };

    if(token) {
        options.headers = {
            ...options.headers,
            ...{'Authorization':token}
        }
    }

    let _req = await fetch(url, options)
  
    let _res = await _req.json();
    return _res;
}

export const GET = async (url:string, token?:string|null) => {
    let options = {
        method: "GET",
        headers: {
          'Content-Type': 'application/json',
        }
    };

    if(token) {
        options.headers = {
            ...options.headers,
            ...{'Authorization':token}
        }
    }

    let _req = await fetch(url, options)
  
    let _res = await _req.json();
    return _res;
}
